# Installing pusheen.js #

You can add this script at the bottom of your `<body>` tag, which will fetch pusheen.js directly from this server:

```
#!html

<script src="https://bitbucket.org/AlphSpirit/pusheen.js/raw/master/pusheen.js"></script>
```

# Launching pusheen.js #

To make the magic happen, simply type in the Konami code in the browser while pusheen.js is loaded:

Keyboard: `Up` `Up` `Down` `Down` `Left` `Right` `Left` `Right` `B` `A`

Touch: `Swipe up` `Swipe up` `Swipe down` `Swipe down` `Swipe left` `Swipe right` `Swipe left` `Swipe right` `Tap` `Tap`

A pusheen should appear on the screen. You can spawn as many as you want!

# Dependencies #

pusheen.js is written in pure Javascript and should be compatible with all modern browsers (which means not Internet Explorer).