/*
 * pusheen.js - Version 1
 * By Alexandre Desbiens
 */

var pusheens = [];
var pusheenIndex = 0;

window.addEventListener('load', function() {
    var k = new Konami();
    k.code = createPusheen;
    k.load();
}, false);

function createPusheen() {
    var pusheen = new Pusheen(pusheenIndex);
    pusheen.init();
    pusheens[pusheenIndex] = pusheen;
    pusheenIndex++;
}

var Pusheen = function(index) {
    
    var pusheen = {
        
        index: index,
        
        init: function() {
            
            // Create div
            //document.body.innerHTML += "<div id='divP" + this.index + "'></div>";
            var div = document.createElement("div");
            div.id = "divP" + this.index;
            document.body.appendChild(div);
            document.getElementById("divP" + this.index).style.position = "fixed";
            document.getElementById("divP" + this.index).style.backgroundSize = "100%";
            document.getElementById("divP" + this.index).style.pointerEvents = "none";
            document.getElementById("divP" + this.index).style.zIndex = "2000";
            document.getElementById("divP" + this.index).innerHTML += "<img id='p_stand" + this.index + "' src='http://bitbucket.org/AlphSpirit/pusheen.js/raw/master/img/p_stand.png' style='width: 100%; height: 100%; display: none;'></img>";
            document.getElementById("divP" + this.index).innerHTML += "<img id='p_walk_1" + this.index + "' src='http://bitbucket.org/AlphSpirit/pusheen.js/raw/master/img/p_walk_1.png' style='width: 100%; height: 100%; display: none;'></img>";
            document.getElementById("divP" + this.index).innerHTML += "<img id='p_walk_2" + this.index + "' src='http://bitbucket.org/AlphSpirit/pusheen.js/raw/master/img/p_walk_2.png' style='width: 100%; height: 100%; display: none;'></img>";
            document.getElementById("divP" + this.index).innerHTML += "<img id='p_walk_3" + this.index + "' src='http://bitbucket.org/AlphSpirit/pusheen.js/raw/master/img/p_walk_3.png' style='width: 100%; height: 100%; display: none;'></img>";
            // Init values
            this.setSize(100, 100);
            this.setPosition(Math.random() * (innerWidth - this.width), Math.random() * (innerHeight - this.height));
            this.setState("stand");
            // Create interval
            this.interval = window.setInterval(this.update.bind(this), 16);
            // Bind events
            document.getElementById("divP" + this.index).addEventListener("transitionend", this.finishedTransition.bind(this), false);
            
        },
        
        update: function() {
            
            // Clipping
            if (this.x < 0) {
                this.setX(0);
            }
            if (this.x + this.width > innerWidth) {
                this.setX(innerWidth - this.width);
            }
            if (this.y < 0) {
                this.setY(0);
            }
            if (this.y + this.height > innerHeight) {
                this.setY(innerHeight - this.height);
            }
            
            // Random movement
            if (Math.random() * 1200 < 1) {
                this.setDestination(
                    Math.random() * (innerWidth - this.width),
                    Math.random() * (innerHeight - this.height),
                    0.5 + Math.random() / 2
                );
            }
            
            // Animation
            this.currentAnimationTime++;
            if (this.currentAnimationTime > this.currentAnimation[this.currentAnimationFrame].time) {
                this.currentAnimationFrame++;
                this.currentAnimationTime = 0;
                if (this.currentAnimationFrame >= this.currentAnimation.length) {
                    this.currentAnimationFrame = 0;
                }
                this.showAnimationFrame(this.currentAnimation[this.currentAnimationFrame].fileName);
            }
            
        },
        
        setX: function(x) {
            this.x = x;
            document.getElementById("divP" + this.index).style.left = x + "px";
        },
        
        setY: function(y) {
            this.y = y;
            document.getElementById("divP" + this.index).style.top = y + "px";
        },
        
        setPosition: function(x, y) {
            this.setX(x);
            this.setY(y);
        },
        
        setSize: function(width, height) {
            this.width = width;
            this.height = height;
            document.getElementById("divP" + this.index).style.width = width + "px";
            document.getElementById("divP" + this.index).style.height = height + "px";
        },
        
        setDestination: function(x, y, speed) {
            if (this.currentState != "walk") {
                var cssSpeed = this.distance(this.x, this.y, x, y) / (speed * 60);
                document.getElementById("divP" + this.index).style.transition = "left " + cssSpeed + "s linear, top " + cssSpeed + "s linear";
                if (x >= this.x) {
                    document.getElementById("divP" + this.index).style.transform = "scaleX(-1)";
                } else {
                    document.getElementById("divP" + this.index).style.transform = "scaleX(1)";
                }
                this.setPosition(x, y);
                this.setState("walk");
            }
        },
        
        setState: function(name) {
            this.currentState = name;
            switch (name) {
                case "stand":
                    this.currentAnimation = [{
                        fileName: "p_stand",
                        time: 60 * 60 * 60
                    }];
                    break;
                case "walk":
                    this.currentAnimation = [{
                        fileName: "p_walk_1",
                        time: 16
                    }, {
                        fileName: "p_walk_2",
                        time: 16
                    }, {
                        fileName: "p_walk_1",
                        time: 16
                    }, {
                        fileName: "p_walk_3",
                        time: 16
                    }];
                    break;
            }
            this.currentAnimationFrame = 0;
            this.currentAnimationTime = 0;
            this.showAnimationFrame(this.currentAnimation[0].fileName);
        },
        
        showAnimationFrame: function(name) {
            if (name == this.currentDisplayedImage) {
                return;
            }
            document.getElementById(name + this.index).style.display = "block";
            if (this.currentDisplayedImage) {
                document.getElementById(this.currentDisplayedImage + this.index).style.display = "none";
            }
            this.currentDisplayedImage = name;
        },
        
        finishedTransition: function(e) {
            this.setState("stand");
        },
        
        distance: function(x1, y1, x2, y2) {
            return Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 -x1, 2));
        },
        
        angle: function(x1, y1, x2, y2) {
            return Math.atan2(y2 - y1, x2 - x1);
        }
        
    }
    
    return pusheen;
    
}

/* Konami code function */
var Konami = function (callback) {
	var konami = {
		addEvent: function (obj, type, fn, ref_obj) {
			if (obj.addEventListener)
				obj.addEventListener(type, fn, false);
			else if (obj.attachEvent) {
				// IE
				obj["e" + type + fn] = fn;
				obj[type + fn] = function () {
					obj["e" + type + fn](window.event, ref_obj);
				}
				obj.attachEvent("on" + type, obj[type + fn]);
			}
		},
		input: "",
		pattern: "38384040373937396665",
		load: function (link) {
			this.addEvent(document, "keydown", function (e, ref_obj) {
				if (ref_obj) konami = ref_obj; // IE
				konami.input += e ? e.keyCode : event.keyCode;
				if (konami.input.length > konami.pattern.length)
					konami.input = konami.input.substr((konami.input.length - konami.pattern.length));
				if (konami.input == konami.pattern) {
					konami.code(link);
					konami.input = "";
					e.preventDefault();
					return false;
				}
			}, this);
			this.iphone.load(link);
		},
		code: function (link) {
			window.location = link
		},
		iphone: {
			start_x: 0,
			start_y: 0,
			stop_x: 0,
			stop_y: 0,
			tap: false,
			capture: false,
			orig_keys: "",
			keys: ["UP", "UP", "DOWN", "DOWN", "LEFT", "RIGHT", "LEFT", "RIGHT", "TAP", "TAP"],
			code: function (link) {
				konami.code(link);
			},
			load: function (link) {
				this.orig_keys = this.keys;
				konami.addEvent(document, "touchmove", function (e) {
					if (e.touches.length == 1 && konami.iphone.capture == true) {
						var touch = e.touches[0];
						konami.iphone.stop_x = touch.pageX;
						konami.iphone.stop_y = touch.pageY;
						konami.iphone.tap = false;
						konami.iphone.capture = false;
						konami.iphone.check_direction();
					}
				});
				konami.addEvent(document, "touchend", function (evt) {
					if (konami.iphone.tap == true) konami.iphone.check_direction(link);
				}, false);
				konami.addEvent(document, "touchstart", function (evt) {
					konami.iphone.start_x = evt.changedTouches[0].pageX;
					konami.iphone.start_y = evt.changedTouches[0].pageY;
					konami.iphone.tap = true;
					konami.iphone.capture = true;
				});
			},
			check_direction: function (link) {
				x_magnitude = Math.abs(this.start_x - this.stop_x);
				y_magnitude = Math.abs(this.start_y - this.stop_y);
				x = ((this.start_x - this.stop_x) < 0) ? "RIGHT" : "LEFT";
				y = ((this.start_y - this.stop_y) < 0) ? "DOWN" : "UP";
				result = (x_magnitude > y_magnitude) ? x : y;
				result = (this.tap == true) ? "TAP" : result;

				if (result == this.keys[0]) this.keys = this.keys.slice(1, this.keys.length);
				if (this.keys.length == 0) {
					this.keys = this.orig_keys;
					this.code(link);
				}
			}
		}
	}
	typeof callback === "string" && konami.load(callback);
	if (typeof callback === "function") {
		konami.code = callback;
		konami.load();
	}
	return konami;
};
